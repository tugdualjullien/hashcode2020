package fr.vif.meuhmes;

import static fr.vif.meuhmes.utils.ArrayUtils.*;
import static fr.vif.meuhmes.utils.Combinations.*;
import static fr.vif.meuhmes.utils.FileOperations.*;
import fr.vif.meuhmes.utils.*;
import org.junit.Test;

/**
 * Copy this class to test another algorithm.
 */
public class DatasetTest {
    private static class Result {

    }

    @Test
    public void applyAlgo() throws Exception {
        apply("a_example.in", this::readDs);
        apply("b_small.in", this::readDs);
        apply("c_medium.in", this::readDs);
        apply("d_quite_big.in", this::readDs);
        apply("e_also_big.in", this::readDs);
    }

    private void readDs(String filename, HashcodeReader reader, HashcodeWriter writer) throws Exception {
        Result res = new Result();
    }
}
