package fr.vif.meuhmes.utils;

import java.io.BufferedWriter;
import java.io.IOException;

public class HashcodeWriter {
    private final BufferedWriter writer;

    private HashcodeWriter(BufferedWriter writer) {
        this.writer = writer;
    }

    public static HashcodeWriter with(BufferedWriter writer) {
        return new HashcodeWriter(writer);
    }

    public HashcodeWriter newLine() throws IOException {
        writer.newLine();
        return this;
    }

    public HashcodeWriter separator() throws IOException {
        writer.write(" ");
        return this;
    }

    public HashcodeWriter add(String value) throws IOException {
        writer.write(value);
        return this;
    }

    public HashcodeWriter add(int value) throws IOException {
        writer.write(Integer.toString(value));
        return this;
    }
}
