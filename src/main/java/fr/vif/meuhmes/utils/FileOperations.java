package fr.vif.meuhmes.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static java.nio.file.Files.newBufferedReader;
import static java.nio.file.Files.newBufferedWriter;

public class FileOperations {

    public static final String[] INPUTS = {"a_example.in",
            "b_small.in",
            "c_medium.in",
            "d_quite_big.in",
            "e_also_big.in"
    };

    @FunctionalInterface
    public interface Reading {
        void with(String filename, HashcodeReader inputReader, HashcodeWriter outputWriter) throws Exception;
    }

    public static void apply(String file, Reading reading) throws Exception {
        URL url = FileOperations.class.getClassLoader().getResource(file);
        if (url != null) {
            Path path = Paths.get(url.toURI());

            Path outputDir = Paths.get("output");
            if (!outputDir.toFile().isDirectory()) {
                Files.createDirectory(outputDir);
            }
            Path outputFile = outputDir.resolve(file.replace(".in", ".out"));
            if (outputFile.toFile().exists()) {
                Files.delete(outputFile);
            }

            try (BufferedReader r = newBufferedReader(path); BufferedWriter w = newBufferedWriter(outputFile)) {
                reading.with(file, HashcodeReader.with(r), HashcodeWriter.with(w));
            }
        }
    }

    public static int index(String value, Map<String, Integer> index) {
        return index.computeIfAbsent(value, v -> index.size());
    }
}
