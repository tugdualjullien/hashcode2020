package fr.vif.meuhmes.utils;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ArrayUtils {
    public static List<Integer> intsToList(int[] ints) {
        return IntStream.of(ints).boxed().collect(Collectors.toList());
    }

    public static int[] listToInts(List<Integer> list) {
        return list.stream().mapToInt(Integer::intValue).toArray();
    }
}
