package fr.vif.meuhmes.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.stream.Stream;

public class HashcodeReader {
    private final BufferedReader reader;

    private HashcodeReader(BufferedReader reader) {
        this.reader = reader;
    }

    public static HashcodeReader with(BufferedReader reader) {
        return new HashcodeReader(reader);
    }

    public String line() throws IOException {
        return reader.readLine();
    }

    public Stream<String> lines() {
        return reader.lines();
    }

    public int[] intsLine() throws IOException {
        return Stream.of(reader.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();
    }

    public String[] stringsLine() throws IOException {
        return reader.readLine().split(" ");
    }
}
