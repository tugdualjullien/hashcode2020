package fr.vif.meuhmes.utils;

public class Combinations {

    @FunctionalInterface
    public interface CombinationHandler {
        void handle(int[] combination);
    }

    public static void combination(int[] src, int k, CombinationHandler handler) {
        recCombination(src, k, 0, new int[k], handler);
    }

    private static void recCombination(int[] src, int k, int start, int[] res, CombinationHandler handler) {
        int n = src.length;
        if (k == 0) {
            handler.handle(res);
            return;
        }
        int resIdx = res.length - k;
        for (int i = start; i <= n-k; i++) {
            res[resIdx] = src[i];
            recCombination(src, k-1, i+1, res, handler);
        }
    }

    public static int combinationsCount(int n, int k) {
        int count = 1;
        // n! / (n-k)! = A(n,k)
        for (int i = n; i > n - k; i--) {
            count *= i;
        }
        // A(n,k) / k!
        for (int i = 2; i <= k; i++) {
            count /= i;
        }
        return count;
    }

}
